import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CashinhandRoutingModule } from './cashinhand-routing.module';

import {  CashinhandComponent } from './cashinhand.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [CashinhandComponent],
  imports: [CommonModule, SharedModule, CashinhandRoutingModule]
})
export class  CashinhandModule {}

