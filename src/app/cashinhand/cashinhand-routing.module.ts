import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CashinhandComponent } from './cashinhand.component';

const routes: Routes = [
  {
    path: 'cashinhand',
    component: CashinhandComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  CashinhandRoutingModule {}
