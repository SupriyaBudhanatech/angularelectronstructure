import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BankaccountRoutingModule } from './bankaccount-routing.module';

import {  BankaccountComponent } from './bankaccount.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [BankaccountComponent],
  imports: [CommonModule, SharedModule, BankaccountRoutingModule]
})
export class  BankaccountModule {}

