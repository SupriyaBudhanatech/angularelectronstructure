import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BankaccountComponent } from './bankaccount.component';

const routes: Routes = [
  {
    path: 'bankaccount',
    component: BankaccountComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  BankaccountRoutingModule {}
