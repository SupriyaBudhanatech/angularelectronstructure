import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SidemenuComponent } from './sidemenu.component';

const routes: Routes = [
  {
    path: 'sidemenu',
    component: SidemenuComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  SidemenuRoutingModule {}
