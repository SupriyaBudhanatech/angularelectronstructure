import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SidemenuRoutingModule } from './sidemenu-routing.module';

import {  SidemenuComponent } from './sidemenu.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SidemenuComponent],
  imports: [CommonModule, SharedModule, SidemenuRoutingModule]
})
export class  SidemenuModule {}

