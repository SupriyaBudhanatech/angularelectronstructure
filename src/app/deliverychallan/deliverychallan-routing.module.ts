import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DeliverychallanComponent } from './deliverychallan.component';

const routes: Routes = [
  {
    path: 'deliverychallan',
    component: DeliverychallanComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  DeliverychallanRoutingModule {}
