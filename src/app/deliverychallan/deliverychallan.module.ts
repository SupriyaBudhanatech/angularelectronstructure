import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliverychallanRoutingModule } from './deliverychallan-routing.module';

import {  DeliverychallanComponent } from './deliverychallan.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [DeliverychallanComponent],
  imports: [CommonModule, SharedModule, DeliverychallanRoutingModule]
})
export class  DeliverychallanModule {}

