import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisteredcompoRoutingModule } from './registeredcompo-routing.module';

import {  RegisteredcompoComponent } from './registeredcompo.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RegisteredcompoComponent],
  imports: [CommonModule, SharedModule, RegisteredcompoRoutingModule]
})
export class  RegisteredcompoModule {}

