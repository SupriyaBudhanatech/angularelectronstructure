import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisteredcompoComponent } from './registeredcompo.component';

const routes: Routes = [
  {
    path: 'registeredcompo',
    component: RegisteredcompoComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  RegisteredcompoRoutingModule {}
