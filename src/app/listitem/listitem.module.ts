import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListitemRoutingModule } from './listitem-routing.module';

import { ListitemComponent } from './listitem.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ListitemComponent],
  imports: [CommonModule, SharedModule, ListitemRoutingModule]
})
export class  ListitemModule {}
