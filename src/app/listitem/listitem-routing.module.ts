import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListitemComponent } from './listitem.component';

const routes: Routes = [
  {
    path: 'listitem',
    component: ListitemComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  ListitemRoutingModule {}
