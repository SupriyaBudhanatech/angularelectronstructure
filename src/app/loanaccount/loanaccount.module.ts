import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoanaccountRoutingModule } from './loanaccount-routing.module';

import {  LoanaccountComponent } from './loanaccount.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [LoanaccountComponent],
  imports: [CommonModule, SharedModule, LoanaccountRoutingModule]
})
export class  LoanaccountModule {}

