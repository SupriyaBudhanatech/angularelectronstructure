import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoanaccountComponent } from './loanaccount.component';

const routes: Routes = [
  {
    path: 'loanaccount',
    component: LoanaccountComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  LoanaccountRoutingModule {}
