import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RestorebackupComponent } from './restorebackup.component';

const routes: Routes = [
  {
    path: 'restorebackup',
    component: RestorebackupComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  RestorebackupRoutingModule {}
