import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestorebackupRoutingModule } from './restorebackup-routing.module';

import {  RestorebackupComponent } from './restorebackup.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RestorebackupComponent],
  imports: [CommonModule, SharedModule, RestorebackupRoutingModule]
})
export class  RestorebackupModule {}

