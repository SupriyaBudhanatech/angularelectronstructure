import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {PurchaseorderRoutingModule } from './purchaseorder-routing.module';

import {  PurchaseorderComponent } from './purchaseorder.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PurchaseorderComponent],
  imports: [CommonModule, SharedModule, PurchaseorderRoutingModule]
})
export class  PurchaseorderModule {}

