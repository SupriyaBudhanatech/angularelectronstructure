import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseorderComponent } from './purchaseorder.component';

const routes: Routes = [
  {
    path: 'purchaseorder',
    component: PurchaseorderComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  PurchaseorderRoutingModule {}
