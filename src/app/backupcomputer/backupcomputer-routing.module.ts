import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BackupcomputerComponent } from './backupcomputer.component';

const routes: Routes = [
  {
    path: 'backupcomputer',
    component: BackupcomputerComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  BackupcomputerRoutingModule {}
