import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BackupcomputerRoutingModule } from './backupcomputer-routing.module';

import {  BackupcomputerComponent } from './backupcomputer.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [BackupcomputerComponent],
  imports: [CommonModule, SharedModule, BackupcomputerRoutingModule]
})
export class  BackupcomputerModule {}
