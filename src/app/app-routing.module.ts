import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';
import { AdditemRoutingModule } from './additem/additem-routing.module';
import { ListitemRoutingModule } from './listitem/listitem-routing.module';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'additem',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AdditemRoutingModule,
    ListitemRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
