import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisteredregularComponent } from './registeredregular.component';

const routes: Routes = [
  {
    path: 'registeredregular',
    component: RegisteredregularComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  RegisteredregularRoutingModule {}
