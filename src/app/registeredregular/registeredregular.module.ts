import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisteredregularRoutingModule } from './registeredregular-routing.module';

import {  RegisteredregularComponent } from './registeredregular.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RegisteredregularComponent],
  imports: [CommonModule, SharedModule, RegisteredregularRoutingModule]
})
export class  RegisteredregularModule {}

