import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PaymentinComponent } from './paymentin.component';

const routes: Routes = [
  {
    path: 'paymentin',
    component: PaymentinComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  PaymentinRoutingModule {}
