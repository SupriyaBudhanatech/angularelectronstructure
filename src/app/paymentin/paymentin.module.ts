import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentinRoutingModule } from './paymentin-routing.module';

import {  PaymentinComponent } from './paymentin.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PaymentinComponent],
  imports: [CommonModule, SharedModule, PaymentinRoutingModule]
})
export class  PaymentinModule {}

