import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PartylistComponent } from './partylist.component';

const routes: Routes = [
  {
    path: 'partylist',
    component: PartylistComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  PartylistRoutingModule {}
