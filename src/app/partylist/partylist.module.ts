import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartylistRoutingModule } from './partylist-routing.module';

import {  PartylistComponent } from './partylist.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PartylistComponent],
  imports: [CommonModule, SharedModule, PartylistRoutingModule]
})
export class  PartylistModule {}

