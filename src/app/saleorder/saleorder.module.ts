import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaleorderRoutingModule } from './saleorder-routing.module';

import {  SaleorderComponent } from './saleorder.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SaleorderComponent],
  imports: [CommonModule, SharedModule, SaleorderRoutingModule]
})
export class  SaleorderModule {}

