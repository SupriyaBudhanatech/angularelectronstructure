import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SaleorderComponent } from './saleorder.component';

const routes: Routes = [
  {
    path: 'saleorder',
    component: SaleorderComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  SaleorderRoutingModule {}
