import 'reflect-metadata';
import '../polyfills';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from  './material.module';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import {MatSelectModule} from '@angular/material/select';
import { AppRoutingModule } from './app-routing.module';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import{SidemenuModule} from './sidemenu/sidemenu.module';
import {ListitemComponent} from './listitem/listitem.component';
import { ListitemModule } from './listitem/listitem.module';
import{AdditemComponent} from './additem/additem.component';
import{AdditemModule} from './additem/additem.module';
import { AddpartyComponent } from './addparty/addparty.component';
import{AddpartyModule} from './addparty/addparty.module'
import { AddsaleComponent } from './addsale/addsale.component';
import{AddsaleModule} from './addsale/addsale.module';
import { ViewitemComponent } from './viewitem/viewitem.component';
import {ViewitemModule} from './viewitem/viewitem.module';
import { UtilitiesComponent } from './utilities/utilities.component';
import{UtilitiesModule} from './utilities/utilities.module';
import { SalereturnComponent } from './salereturn/salereturn.component';
import {SalereturnModule} from './salereturn/salereturn.module';
import { SaleorderComponent } from './saleorder/saleorder.component';
import {SaleorderModule} from './saleorder/saleorder.module';
import { SaleinvoicesComponent } from './saleinvoices/saleinvoices.component';
import {SaleinvoicesModule} from './saleinvoices/saleinvoices.module';
import { RestorebackupComponent } from './restorebackup/restorebackup.component';
import {RestorebackupModule} from './restorebackup/restorebackup.module';
import { ReportComponent } from './report/report.component';
import{ReportMdule} from './report/report.module';
import { PurchasereturnComponent } from './purchasereturn/purchasereturn.component';
import{PurchasereturnModule} from './purchasereturn/purchasereturn.module'
import { PurchaseorderComponent } from './purchaseorder/purchaseorder.component';
import{PurchaseorderModule} from './purchaseorder/purchaseorder.module';
import { PurchasebillsComponent } from './purchasebills/purchasebills.component';
import {PurchasebillsModule} from './purchasebills/purchasebills.module';
import { PaymentoutComponent } from './paymentout/paymentout.component';
import { PaymentinComponent } from './paymentin/paymentin.component';
import{PaymentinModule} from './paymentin/paymentin.module';
import { LoanaccountComponent } from './loanaccount/loanaccount.component';
import{LoanaccountModule} from './loanaccount/loanaccount.module'
import { ExpensesComponent } from './expenses/expenses.component';
import{ExpensesModule} from './expenses/expenses.module';
import { EstimationComponent } from './estimation/estimation.component';
import {EstimationModule} from './estimation/estimation.module';
import { EditsaleComponent } from './editsale/editsale.component';
import{EditsaleModule} from './editsale/editsale.module';
import { DeliverychallanComponent } from './deliverychallan/deliverychallan.component';
import{DeliverychallanModule} from './deliverychallan/deliverychallan.module';
import { ChequeComponent } from './cheque/cheque.component';
import{ChequeModule} from './cheque/cheque.module';
import { CashinhandComponent } from './cashinhand/cashinhand.component';
import{CashinhandModule} from './cashinhand/cashinhand.module';
import { BankaccountComponent } from './bankaccount/bankaccount.component';
import {BankaccountModule} from './bankaccount/bankaccount.module';
import { BackupdriveComponent } from './backupdrive/backupdrive.component';
import {BackupdriveModule} from './backupdrive/backupdrive.module';
import { BackupcomputerComponent } from './backupcomputer/backupcomputer.component';
import{BackupcomputerModule} from './backupcomputer/backupcomputer.module';
import { UnregisterComponent } from './unregister/unregister.component';
import{UnregisterModule} from './unregister/unregister.module';
import { RegisteredregularComponent } from './registeredregular/registeredregular.component';
import {RegisteredregularModule} from './registeredregular/registeredregular.module';
import { RegisteredcompoComponent } from './registeredcompo/registeredcompo.component';
import {RegisteredcompoModule} from './registeredcompo/registeredcompo.module';

const  appRoutes:  Routes  = [
  { path:  'listitem', component:  ListitemComponent },
  { path:  'addparty', component:  AddpartyComponent },
  { path:  'addsale', component:  AddsaleComponent },
  { path:  'viewitem', component:  ViewitemComponent },
  { path:  'utilities', component:  UtilitiesComponent },
  { path:  'salereturn', component:  SalereturnComponent },
  { path:  'saleorder', component:  SaleorderComponent },
  { path:  'saleinvoices', component:  SaleinvoicesComponent },
  { path:  'report', component:  ReportComponent },
  { path:  'purchasereturn', component: PurchasereturnComponent },
  { path:  'purchaseorder', component:  PurchaseorderComponent },
  { path:  'purchasebills', component:  PurchasebillsComponent },
  { path:  'paymentout', component:  PaymentoutComponent },
  { path:  'paymentin', component: PaymentinComponent },
  { path:  'loanaccount', component:  LoanaccountComponent },
  { path:  'expenses', component:  ExpensesComponent },
  { path:  'estimation', component:  EstimationComponent },
  { path:  'editsale', component:  EditsaleComponent },
  { path:  'deliverychallan', component:  DeliverychallanComponent },
  { path:  'cheque', component:  ChequeComponent },
  { path:  'cashinhand', component:  CashinhandComponent },
  { path:  'bankaccount', component:  BankaccountComponent },
  { path:  'backupdrive', component:  BackupdriveComponent },
  { path:  'backupcomputer', component:  BackupcomputerComponent },
  { path:  'unregister', component:  UnregisterComponent },
  { path:  'registeredregular', component:  RegisteredregularComponent },
  { path:  'registeredcompo', component:  RegisteredcompoComponent },
  { path:  'restorebackup', component:  RestorebackupComponent },

  { path:  'additem', component:  AdditemComponent },
  {
  path:  'sidemenu',
  component:  SidemenuComponent
  },
  { path:  '',
  
  redirectTo:  '/sidemenu',
  
  pathMatch:  'full'
  
  },
  ];
  

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSelectModule,
    FormsModule,
    ListitemModule,
    AdditemModule,
    AddpartyModule,
    BankaccountModule,
    EditsaleModule,
    SaleorderModule,
    AddsaleModule,
    HttpClientModule,
    SalereturnModule,
    LoanaccountModule,
    UtilitiesModule,
    RestorebackupModule,
    BackupdriveModule,
    SidemenuModule,
    ExpensesModule,
    PurchasereturnModule,
    CoreModule,
    SharedModule,
    PurchaseorderModule,
    EstimationModule,
    ChequeModule,
    RegisteredcompoModule,
    PurchasebillsModule,
    BackupcomputerModule,
    DeliverychallanModule,
    SaleinvoicesModule,
    PaymentinModule,
    RegisteredregularModule,
    UnregisterModule,
    MaterialModule,
    AppRoutingModule,
    ReportMdule,
    RouterModule, 
    CashinhandModule,
    ViewitemModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],


  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
