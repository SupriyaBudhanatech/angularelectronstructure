import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddpartyRoutingModule } from './addparty-routing.module';

import { AddpartyComponent } from './addparty.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AddpartyComponent],
  imports: [CommonModule, SharedModule, AddpartyRoutingModule]
})
export class  AddpartyModule {}
