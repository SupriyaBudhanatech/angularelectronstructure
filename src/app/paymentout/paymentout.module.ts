import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentoutRoutingModule } from './paymentout-routing.module';

import {  PaymentoutComponent } from './paymentout.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PaymentoutComponent],
  imports: [CommonModule, SharedModule, PaymentoutRoutingModule]
})
export class PaymentoutModule {}

