import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PaymentoutComponent } from './paymentout.component';

const routes: Routes = [
  {
    path: 'paymentout',
    component: PaymentoutComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class   PaymentoutRoutingModule {}
