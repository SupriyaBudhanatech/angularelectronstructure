import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PurchasereturnComponent } from './purchasereturn.component';

const routes: Routes = [
  {
    path: 'purchasereturn',
    component: PurchasereturnComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  PurchasereturnComponentRoutingModule {}
