import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchasereturnComponentRoutingModule } from './purchasereturn-routing.module';

import {  PurchasereturnComponent } from './purchasereturn.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PurchasereturnComponent],
  imports: [CommonModule, SharedModule, PurchasereturnComponentRoutingModule]
})
export class  PurchasereturnModule {}

