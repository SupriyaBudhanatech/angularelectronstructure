import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ViewitemComponent } from './viewitem.component';

const routes: Routes = [
  {
    path: 'viewitem',
    component: ViewitemComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  ViewitemRoutingModule {}
