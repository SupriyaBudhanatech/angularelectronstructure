import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewitemRoutingModule } from './viewitem-routing.module';

import {  ViewitemComponent } from './viewitem.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ViewitemComponent],
  imports: [CommonModule, SharedModule, ViewitemRoutingModule]
})
export class  ViewitemModule {}

