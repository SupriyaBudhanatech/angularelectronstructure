import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddsaleRoutingModule } from './addsale-routing.module';

import { AddsaleComponent } from './addsale.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AddsaleComponent],
  imports: [CommonModule, SharedModule, AddsaleRoutingModule]
})
export class  AddsaleModule {}
