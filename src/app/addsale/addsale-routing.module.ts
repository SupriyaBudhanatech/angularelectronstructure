import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddsaleComponent } from './addsale.component';

const routes: Routes = [
  {
    path: 'addsale',
    component: AddsaleComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  AddsaleRoutingModule {}
