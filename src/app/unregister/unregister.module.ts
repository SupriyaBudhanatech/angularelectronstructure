import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnregisterRoutingModule } from './Unregister-routing.module';

import {  UnregisterComponent } from './unregister.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [UnregisterComponent],
  imports: [CommonModule, SharedModule, UnregisterRoutingModule]
})
export class  UnregisterModule {}

