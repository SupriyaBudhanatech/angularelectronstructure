import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UnregisterComponent } from './unregister.component';

const routes: Routes = [
  {
    path: 'unregister',
    component: UnregisterComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  UnregisterRoutingModule {}

 