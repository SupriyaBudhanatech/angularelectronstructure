import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtilitiesRoutingModule } from './utilities-routing.module';

import {   UtilitiesComponent } from './utilities.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [UtilitiesComponent],
  imports: [CommonModule, SharedModule, UtilitiesRoutingModule]
})
export class   UtilitiesModule {}

