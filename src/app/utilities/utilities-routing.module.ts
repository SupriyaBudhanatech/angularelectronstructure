import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UtilitiesComponent } from './utilities.component';

const routes: Routes = [
  {
    path: 'utilities',
    component: UtilitiesComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  UtilitiesRoutingModule {}
