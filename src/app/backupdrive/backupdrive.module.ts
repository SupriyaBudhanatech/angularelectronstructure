import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BackupdriveRoutingModule } from './backupdrive-routing.module';

import {  BackupdriveComponent } from './backupdrive.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [BackupdriveComponent],
  imports: [CommonModule, SharedModule, BackupdriveRoutingModule]
})
export class  BackupdriveModule {}

