import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BackupdriveComponent } from './backupdrive.component';

const routes: Routes = [
  {
    path: 'backupdrive',
    component: BackupdriveComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  BackupdriveRoutingModule {}
