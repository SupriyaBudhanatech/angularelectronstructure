import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdditemRoutingModule } from './additem-routing.module';

import { AdditemComponent } from './additem.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AdditemComponent],
  imports: [CommonModule, SharedModule, AdditemRoutingModule]
})
export class  AdditemModule {}
