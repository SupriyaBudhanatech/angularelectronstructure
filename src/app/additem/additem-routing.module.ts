import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdditemComponent } from './additem.component';

const routes: Routes = [
  {
    path: 'additem',
    component: AdditemComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  AdditemRoutingModule {}
