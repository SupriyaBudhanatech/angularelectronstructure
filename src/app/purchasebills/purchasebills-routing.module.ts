import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PurchasebillsComponent } from './purchasebills.component';

const routes: Routes = [
  {
    path: 'purchasebills',
    component: PurchasebillsComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  PurchasebillsRoutingModule {}
