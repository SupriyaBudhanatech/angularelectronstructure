import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchasebillsRoutingModule } from './purchasebills-routing.module';

import {  PurchasebillsComponent } from './purchasebills.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PurchasebillsComponent],
  imports: [CommonModule, SharedModule, PurchasebillsRoutingModule]
})
export class  PurchasebillsModule {}

