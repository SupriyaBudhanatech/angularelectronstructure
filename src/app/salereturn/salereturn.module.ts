import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalereturnRoutingModule } from './salereturn-routing.module';

import {  SalereturnComponent } from './salereturn.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SalereturnComponent],
  imports: [CommonModule, SharedModule, SalereturnRoutingModule]
})
export class  SalereturnModule {}

