import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SalereturnComponent } from './salereturn.component';

const routes: Routes = [
  {
    path: 'salereturn',
    component: SalereturnComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  SalereturnRoutingModule {}
