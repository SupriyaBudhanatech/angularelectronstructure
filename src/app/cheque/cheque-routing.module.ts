import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ChequeComponent } from './cheque.component';

const routes: Routes = [
  {
    path: 'cheque',
    component: ChequeComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  ChequeRoutingModule {}
