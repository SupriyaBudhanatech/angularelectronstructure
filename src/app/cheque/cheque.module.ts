import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChequeRoutingModule } from './cheque-routing.module';

import {  ChequeComponent } from './cheque.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ChequeComponent],
  imports: [CommonModule, SharedModule, ChequeRoutingModule]
})
export class  ChequeModule {}

