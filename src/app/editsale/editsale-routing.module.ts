import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EditsaleComponent } from './editsale.component';

const routes: Routes = [
  {
    path: 'editsale',
    component: EditsaleComponent

  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  EditsaleRoutingModule {}
