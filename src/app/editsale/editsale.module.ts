import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditsaleRoutingModule } from './editsale-routing.module';

import {  EditsaleComponent } from './editsale.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [EditsaleComponent],
  imports: [CommonModule, SharedModule, EditsaleRoutingModule]
})
export class  EditsaleModule {}

