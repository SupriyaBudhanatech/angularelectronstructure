import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EstimationComponent } from './estimation.component';

const routes: Routes = [
  {
    path: 'estimation',
    component: EstimationComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  EstimationRoutingModule {}
