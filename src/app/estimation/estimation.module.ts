import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstimationRoutingModule } from './estimation-routing.module';

import {  EstimationComponent } from './estimation.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [EstimationComponent],
  imports: [CommonModule, SharedModule, EstimationRoutingModule]
})
export class  EstimationModule {}

