import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SaleinvoicesComponent } from './saleinvoices.component';

const routes: Routes = [
  {
    path: 'saleinvoices',
    component: SaleinvoicesComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class  SaleinvoicesRoutingModule {}
