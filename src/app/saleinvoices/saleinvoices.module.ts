import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaleinvoicesRoutingModule } from './saleinvoices-routing.module';

import {  SaleinvoicesComponent } from './saleinvoices.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SaleinvoicesComponent],
  imports: [CommonModule, SharedModule, SaleinvoicesRoutingModule]
})
export class  SaleinvoicesModule {}

